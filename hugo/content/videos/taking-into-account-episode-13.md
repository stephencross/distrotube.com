---
title: "Taking Into Account, Ep. 13 - Linus is back, Snaps, Ubuntu stats, Microsoft patents, Arch Linux"
image: images/thumbs/0301.jpg
date: Thu, 25 Oct 2018 00:02:06 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+13.mp4" >}}
&nbsp;

#### SHOW NOTES

<a href="https://www.youtube.com/watch?v=TYovYGdoQoQ&amp;t=44s">0:44 Linus Torvalds returns to Linux after his self-imposed break. 

<a href="https://www.youtube.com/watch?v=TYovYGdoQoQ&amp;t=494s">8:14 Snap packages are a massive success! 

<a href="https://www.youtube.com/watch?v=TYovYGdoQoQ&amp;t=868s">14:28 The Ubuntu user statistics webpage posts data collected from users who opted in. 

<a href="https://www.youtube.com/watch?v=TYovYGdoQoQ&amp;t=1293s">21:33 What's the deal with Microsoft's open-source friendly patents? 

<a href="https://www.youtube.com/watch?v=TYovYGdoQoQ&amp;t=1656s">27:36 Arch Linux wasn't always this popular.  Why is that? 

<a href="https://www.youtube.com/watch?v=TYovYGdoQoQ&amp;t=2156s">35:56 I read a question from a follower on Mastodon. 

REFERENCED IN THE VIDEO: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=TYovYGdoQoQ&amp;redir_token=zFPzxcOpyBNnv_KMC_l7ycOIcSt8MTU1MzQ3MjMxMkAxNTUzMzg1OTEy&amp;q=https%3A%2F%2Fwww.theverge.com%2F2018%2F10%2F22%2F18011854%2Flinus-torvalds-linux-kernel-development-return-code-of-conduct" target="_blank">https://www.theverge.com/2018/10/22/1...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=TYovYGdoQoQ&amp;redir_token=zFPzxcOpyBNnv_KMC_l7ycOIcSt8MTU1MzQ3MjMxMkAxNTUzMzg1OTEy&amp;q=https%3A%2F%2Fbetanews.com%2F2018%2F10%2F19%2Fsnap-linux-wow%2F" target="_blank">https://betanews.com/2018/10/19/snap-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=TYovYGdoQoQ&amp;redir_token=zFPzxcOpyBNnv_KMC_l7ycOIcSt8MTU1MzQ3MjMxMkAxNTUzMzg1OTEy&amp;q=https%3A%2F%2Fwww.ubuntu.com%2Fdesktop%2Fstatistics" target="_blank">https://www.ubuntu.com/desktop/statis...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=TYovYGdoQoQ&amp;redir_token=zFPzxcOpyBNnv_KMC_l7ycOIcSt8MTU1MzQ3MjMxMkAxNTUzMzg1OTEy&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fwhats-the-deal-with-microsofts-open-source-friendly-patents%2F" target="_blank">https://www.zdnet.com/article/whats-t...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=TYovYGdoQoQ&amp;redir_token=zFPzxcOpyBNnv_KMC_l7ycOIcSt8MTU1MzQ3MjMxMkAxNTUzMzg1OTEy&amp;q=https%3A%2F%2Flists.fedoraproject.org%2Farchives%2Flist%2Flegal%40lists.fedoraproject.org%2Fmessage%2FO67NSBH4X6FD2HHXFUK2I246Z37IYL5F%2F" target="_blank">https://lists.fedoraproject.org/archi...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=TYovYGdoQoQ&amp;redir_token=zFPzxcOpyBNnv_KMC_l7ycOIcSt8MTU1MzQ3MjMxMkAxNTUzMzg1OTEy&amp;q=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FSubpixel_rendering" target="_blank">https://en.wikipedia.org/wiki/Subpixe...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=TYovYGdoQoQ&amp;redir_token=zFPzxcOpyBNnv_KMC_l7ycOIcSt8MTU1MzQ3MjMxMkAxNTUzMzg1OTEy&amp;q=https%3A%2F%2Fwww.reddit.com%2Fr%2Flinux%2Fcomments%2F9qybe3%2Fwhy_was_arch_initially_not_as_popular_as_it_is%2F" target="_blank">https://www.reddit.com/r/linux/commen...
