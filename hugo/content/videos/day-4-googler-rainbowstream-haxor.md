---
title: "Day 4 - Googler, Rainbowstream and Haxor News"
image: images/thumbs/0177.jpg
date: Sat, 14 Apr 2018 22:32:30 +0000
author: Derek Taylor
tags: ["terminal"]
---

#### VIDEO

{{< amazon src="Day+4+-+Googler%2C+Rainbowstream+and+Haxor+News.mp4" >}}
&nbsp;

#### SHOW NOTES

Day 4 of my 30 challenge. I'm living in a tiling window manager for the next 30 days and only using terminal-based applications where possible. I asked you guys for CLI app suggestions and you guys really come through. I share three terminal-based programs that I found interesting. 
