---
title: "Laptop With Intel i7, 4GB of RAM and a 256GB SSD - Under $200"
image: images/thumbs/0233.jpg
date: Wed, 20 Jun 2018 00:09:46 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Laptop+With+Intel+i7%2C+4GB+of+RAM+and+a+256GB+SSD+-+Under+%24200.mp4" >}}
&nbsp;

#### SHOW NOTES

Bought a used laptop off of eBay. A Lenovo Thinkpad e535 with an i7 processor and 4 gigs of RAM for $100 (no drive and no OS). I bought a 256GB SSD drive off of Amazon for $55. Oh, and it runs Linux now!
