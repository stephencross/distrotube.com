---
title: "Welcome To WindowzTube"
image: images/thumbs/0584.jpg
date: 2020-04-01T12:22:40+06:00
author: Derek Taylor
tags: ["Windows", ""]
---

#### VIDEO

{{< amazon src="Welcome+To+WindowzTube.mp4" >}}
&nbsp;

#### SHOW NOTES

Welcome to WindowzTube, your source for all things Windows.  This channel focuses on Microsoft exceptionalism and aims to spread Windows awareness.  Windows is the best operating system on the planet, and though it may not be perfect, in the words of Bill Gates "Life is not fair get, used to it!"

REFERENCED:
+ https://www.microsoft.com/en-us/windows/