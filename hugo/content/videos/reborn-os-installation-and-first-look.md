---
title: "Reborn OS Installation and First Look"
image: images/thumbs/0210.jpg
date: Sat, 19 May 2018 23:25:44 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Reborn OS"]
---

#### VIDEO

{{< amazon src="Reborn+OS+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

I take a quick look at Reborn OS, an Arch-based Linux distro that offers a choice of a dozen different desktop environments in the installer. https://rebornos.wordpress.com/
