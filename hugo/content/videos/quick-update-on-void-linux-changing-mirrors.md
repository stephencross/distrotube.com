---
title: "Quick Update on Void Linux - Changing Mirrors and Octoxbps"
image: images/thumbs/0080.jpg
date: Wed, 03 Jan 2018 01:03:14 +0000
author: Derek Taylor
tags: ["Void Linux", ""]
---

#### VIDEO

{{< amazon src="Quick+Update+on+Void+Linux+-+Changing+Mirrors+and+Octoxbps.mp4" >}}  
&nbsp;

#### SHOW NOTES

This is a quick update video on my Void Linux First Impression Install and Review video from a couple of days ago. I had a few issues during that review that I wanted to revisit. Thanks to the viewers who commented in the previous video. You guys were very helpful. <a href="https://www.voidlinux.eu/">https://www.voidlinux.eu/</a>
