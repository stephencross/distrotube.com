---
title: "Command Line Shenanigans - Tomfoolery In The Terminal"
image: images/thumbs/0379.jpg
date: Tue, 09 Apr 2019 02:47:46 +0000
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Command+Line+Shenanigans+Tomfoolery+In+The+Terminal.mp4" >}}
&nbsp;

#### SHOW NOTES

Funny command line utilities. Some of these I have discussed in prior videos. Some I have not discussed before. Those that I have covered before, I show you some of the options available to you, to get the most fun out of them!
