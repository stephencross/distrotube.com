---
title: "Glances - Terminal-Based System Monitoring Tool"
image: images/thumbs/0039.jpg
date: Thu, 23 Nov 2017 14:22:03 +0000
author: Derek Taylor
tags: ["TUI Apps", "glances"]
---

#### VIDEO

{{< amazon src="Glances+-+Terminal-Based+System+Monitoring+Tool.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at "glances", a terminal-based system monitoring tool that gives information on your system's cpu(s), memory, disk space, processes, network, etc. <a href="https://nicolargo.github.io/glances/">https://nicolargo.github.io/glances/</a>
