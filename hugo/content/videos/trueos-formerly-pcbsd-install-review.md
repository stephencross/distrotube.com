---
title: "TrueOS (formerly PC-BSD) First Impression Install & Review"
image: images/thumbs/0029.jpg
date: Sun, 05 Nov 2017 02:34:14 +0000
author: Derek Taylor
tags: ["Distro Reviews", "TrueOS", "BSD"]
---

#### VIDEO

{{< amazon src="TruesOS+(formerly+PC-BSD)+First+Impression+Install+%26+Review.mp4" >}}Today I install and review the TrueOS BSD distribution with the Lumina desktop environment.  I have limited experience with BSD and have never used TrueOS so this should be interesting.    
