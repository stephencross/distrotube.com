---
title: "Broot Is A Better Way To Navigate Directories"
image: images/thumbs/0534.jpg
date: 2020-02-03T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Broot+Is+A+Better+Way+To+Navigate+Directories.mp4" >}}
&nbsp;

#### SHOW NOTES

Broot is a free and open source command line utility that is a great alternative to the standard "tree" command.  It allows you to view the directory structure, perform fuzzy searches, run commands on the selected files or directories, and much more.  Broot can replace your "ls" command.

REFERENCED:
+ https://github.com/Canop/broot