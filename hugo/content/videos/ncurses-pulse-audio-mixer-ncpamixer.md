---
title: "NCurses Pulse Audio Mixer (ncpamixer)"
image: images/thumbs/0318.jpg
date: Wed, 19 Dec 2018 19:33:48 +0000
author: Derek Taylor
tags: ["TUI Apps", ""]
---

#### VIDEO

{{< amazon src="NCurses+Pulse+Audio+Mixer+(ncpamixer).mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm going to share with you a neat little ncurses program that is
 essentially "pavucontrol" in the terminal.  That program is 
"ncpamixer".  

<a href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2Ffulhax%2Fncpamixer&amp;redir_token=sqbPYIprWYVbPoVB94ssrPunMnx8MTU1MzU0MjQyMkAxNTUzNDU2MDIy&amp;event=video_description&amp;v=7GZ5Er3ZNrQ" rel="noreferrer noopener" target="_blank">https://github.com/fulhax/ncpamixer
