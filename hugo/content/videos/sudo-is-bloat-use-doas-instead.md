---
title: "Sudo Is Bloat. Use Doas Instead!"
image: images/thumbs/0551.jpg
date: 2020-02-21T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Sudo+Is+Bloat+Use+Doas+Instead.mp4" >}}
&nbsp;

#### SHOW NOTES

The doas utility is a program originally written for OpenBSD which allows a user to run a command as though they were another user. Typically doas is used to allow non-privleged users to run commands as though they were the root user. The doas program acts as an alternative to sudo, which is a popular method in the Linux community for granting admin access to specific users.


REFERENCED:
+ https://www.archlinux.org/packages/community/x86_64/opendoas/ - In the official Arch repos.
+ https://github.com/slicer69/doas - In the AUR.