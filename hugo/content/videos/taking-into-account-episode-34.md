---
title: "Taking Into Account, Ep. 34 - Stadia, Open Source Money, Windows 7, Stallman, App Releases, IRC"
image: images/thumbs/0366.jpg
date: Thu, 21 Mar 2019 23:12:45 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+34.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=RlMGv7OFrok&amp;t=55s">0:55 Stadia is Google’s new gaming service powered by Linux and open source technology. 

<a href="https://www.youtube.com/watch?v=RlMGv7OFrok&amp;t=415s">6:55 Open Source doesn’t make money because it isn’t designed to make money.  Is this really true? 

<a href="https://www.youtube.com/watch?v=RlMGv7OFrok&amp;t=907s">15:07 Still on Windows 7?  A new nag screen arrives: "Get Windows 10." Why not move to Linux instead? 

<a href="https://www.youtube.com/watch?v=RlMGv7OFrok&amp;t=1111s">18:31 Stallman on install fests and non-free distros. What to do about the deal with the devil? 

<a href="https://www.youtube.com/watch?v=RlMGv7OFrok&amp;t=1458s">24:18 New app releases: Geary 3.32, OpenShot 2.4.4 and KeePassXC 2.4.0 released. 

<a href="https://www.youtube.com/watch?v=RlMGv7OFrok&amp;t=1678s">27:58 Have you checked out the new Distrotube IRC channel? 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=HTZFhPiPhJEFr5sZbLR2dmk61Ud8MTU1MzY0MTk2OEAxNTUzNTU1NTY4&amp;v=RlMGv7OFrok&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F03%2Fgoogles-new-game-service-is-based-on-linux-open-source-tech&amp;event=video_description" target="_blank">https://www.omgubuntu.co.uk/2019/03/g...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=HTZFhPiPhJEFr5sZbLR2dmk61Ud8MTU1MzY0MTk2OEAxNTUzNTU1NTY4&amp;v=RlMGv7OFrok&amp;q=http%3A%2F%2Fwww.ianbicking.org%2Fblog%2F2019%2F03%2Fopen-source-doesnt-make-money-by-design.html&amp;event=video_description" target="_blank">http://www.ianbicking.org/blog/2019/0...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=HTZFhPiPhJEFr5sZbLR2dmk61Ud8MTU1MzY0MTk2OEAxNTUzNTU1NTY4&amp;v=RlMGv7OFrok&amp;q=https%3A%2F%2Fbetanews.com%2F2019%2F03%2F20%2Fnew-get-windows-10-nag-screen-arrives-in-windows-7%2F&amp;event=video_description" target="_blank">https://betanews.com/2019/03/20/new-g...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=HTZFhPiPhJEFr5sZbLR2dmk61Ud8MTU1MzY0MTk2OEAxNTUzNTU1NTY4&amp;v=RlMGv7OFrok&amp;q=https%3A%2F%2Fwww.gnu.org%2Fphilosophy%2Finstall-fest-devil&amp;event=video_description" target="_blank">https://www.gnu.org/philosophy/instal...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=HTZFhPiPhJEFr5sZbLR2dmk61Ud8MTU1MzY0MTk2OEAxNTUzNTU1NTY4&amp;v=RlMGv7OFrok&amp;q=https%3A%2F%2Fmail.gnome.org%2Farchives%2Fgnome-announce-list%2F2019-March%2Fmsg00014.html&amp;event=video_description" target="_blank">https://mail.gnome.org/archives/gnome...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=HTZFhPiPhJEFr5sZbLR2dmk61Ud8MTU1MzY0MTk2OEAxNTUzNTU1NTY4&amp;v=RlMGv7OFrok&amp;q=https%3A%2F%2Fwww.openshot.org%2Fblog%2F2019%2F03%2F20%2Fopenshot-244-released-keyframe-scaling-docking-and-more%2F&amp;event=video_description" target="_blank">https://www.openshot.org/blog/2019/03...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=HTZFhPiPhJEFr5sZbLR2dmk61Ud8MTU1MzY0MTk2OEAxNTUzNTU1NTY4&amp;v=RlMGv7OFrok&amp;q=https%3A%2F%2Fkeepassxc.org%2F&amp;event=video_description" target="_blank">https://keepassxc.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=HTZFhPiPhJEFr5sZbLR2dmk61Ud8MTU1MzY0MTk2OEAxNTUzNTU1NTY4&amp;v=RlMGv7OFrok&amp;q=https%3A%2F%2Ffreenode.net%2F&amp;event=video_description" target="_blank">https://freenode.net/
