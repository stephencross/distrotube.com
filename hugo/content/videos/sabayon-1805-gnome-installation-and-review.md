---
title: "Sabayon 18.05 GNOME Install and Review"
image: images/thumbs/0180.jpg
date: Tue, 17 Apr 2018 22:37:23 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Sabayon", "GNOME"]
---

#### VIDEO

{{< amazon src="Sabayon+18.05+GNOME+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I do a quick install and overview of the recently released Sabayon Linux 18.05. Sabayon is a Gentoo-based Linux distribution that offers a number of different editions. The one I review today sports the GNOME desktop environment. <a href="https://www.sabayon.org/">https://www.sabayon.org/</a>
