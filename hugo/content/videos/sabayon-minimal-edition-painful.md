---
title: "Sabayon Minimal Edition - Most Painful Linux Install Ever?"
image: images/thumbs/0009.jpg
date: Fri, 13 Oct 2017 01:48:06 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Sabayon"]
---

#### VIDEO

{{< amazon src="Sabayon+Minimal+Edition+-+Most+Painful+Linux+Install+Ever.mp4" >}}  
&nbsp;

#### SHOW NOTES

Another rolling release distro and another first impression review. This time we look at Sabayon Linux based on Gentoo. This install took me almost two hours and I still don't have a full desktop environment installed or very many programs. This install was not pleasant.
