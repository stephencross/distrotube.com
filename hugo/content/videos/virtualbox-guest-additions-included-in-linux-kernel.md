---
title: "Virtualbox Guest Additions To Be Included In Linux Kernel"
image: images/thumbs/0090.jpg
date: Mon, 17 Jan 2018 01:16:54 +0000
author: Derek Taylor
tags: ["Virtualbox"]
---

#### VIDEO

{{< amazon src="Virtualbox+Guest+Additions+To+Be+Included+In+Linux+Kernel.mp4" >}}
&nbsp;

#### SHOW NOTES

Joey Sneddon over at OMG! Ubuntu! recently published a story about the Virtualbox guest additions being included in the Linux kernel starting with kernel verision 4.16.  This is exicting news to me (I install ALOT of virtual machines in Virtualbox) and I just felt like sharing the news. 

https://www.omgubuntu.co.uk/2018/01/virtualbox-guest-additions-linux-kernel
