---
title: "Obscure Window Manager Project - Fluxbox"
image: images/thumbs/0195.jpg
date: Fri, 04 May 2018 22:57:06 +0000
author: Derek Taylor
tags: ["Fluxbox", ""]
---

#### VIDEO

{{< amazon src="Obscure+Window+Manager+Project+-+Fluxbox.mp4" >}}
&nbsp;

#### SHOW NOTES

In this edition of the Obscure Window Manager Project, I take a quick look at Fluxbox. It is a floating window manager that is similar in look and feel to Openbox. http://fluxbox.org/
