---
title: "Sabayon 19.03 GNOME - DT Live"
image: images/thumbs/0375.jpg
date: Tue, 02 Apr 2019 02:36:30 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Sabayon", "GNOME", "Live Stream"]
---

#### VIDEO

{{< amazon src="Sabayon+19.03+GNOME+-+DT+Live-MNHc-8zmaAs.webm" >}}
&nbsp;

#### SHOW NOTES

I am going to do an installation and first look at Sabayon 19.03 GNOME.   Sabayon has always been a favorite distro of mine to play around with.   And GNOME has always been...GNOME.  :D  Let's check it out live! <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.sabayon.org%2F&amp;v=MNHc-8zmaAs&amp;event=video_description&amp;redir_token=eKMqnqprcjamBcReDQ6J7XmhnlV8MTU1NzE5NjY0MUAxNTU3MTEwMjQx" target="_blank">https://www.sabayon.org/
