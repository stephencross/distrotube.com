---
title: "Hacking on Xmonad - GridSelect, ToggleStruts, ToggleBorders"
image: images/thumbs/0448.jpg
date: Sat, 12 Oct 2019 23:55:00 +0000
author: Derek Taylor
tags: ["tiling window managers", "xmonad"]
---

#### VIDEO

{{< amazon src="Hacking+on+Xmonad+GridSelect+ToggleStruts+ToggleBorders.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm just playing with Xmonad and having fun hacking on it. I discuss how to toggle on/off borders and struts in Xmonad for that perfect fullscreen viewing experience. I also show off a neat little tool called gridSelect.

#### REFERENCED:
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=qQ4CcMTLW-4&amp;event=video_description&amp;q=https%3A%2F%2Fhackage.haskell.org%2Fpackage%2Fxmonad-contrib-0.15%2Fdocs%2FXMonad-Hooks-ManageDocks.html&amp;redir_token=XlrrTug7gzwMzrFgbtFswmXeBgd8MTU3NzQ5MDk3MUAxNTc3NDA0NTcx" target="_blank" rel="nofollow noopener noreferrer">https://hackage.haskell.org/package/x...
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=qQ4CcMTLW-4&amp;event=video_description&amp;q=https%3A%2F%2Fhackage.haskell.org%2Fpackage%2Fxmonad-contrib-0.7%2Fdocs%2FXMonad-Layout-NoBorders.html&amp;redir_token=XlrrTug7gzwMzrFgbtFswmXeBgd8MTU3NzQ5MDk3MUAxNTc3NDA0NTcx" target="_blank" rel="nofollow noopener noreferrer">https://hackage.haskell.org/package/x...
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=qQ4CcMTLW-4&amp;event=video_description&amp;q=https%3A%2F%2Fhackage.haskell.org%2Fpackage%2Fxmonad-contrib-0.15%2Fdocs%2FXMonad-Actions-GridSelect.html&amp;redir_token=XlrrTug7gzwMzrFgbtFswmXeBgd8MTU3NzQ5MDk3MUAxNTc3NDA0NTcx" target="_blank" rel="nofollow noopener noreferrer">https://hackage.haskell.org/package/x...
