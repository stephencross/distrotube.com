---
title: "Taking Into Account, Ep. 21 - Debian SJWs, Ubuntu on Dell, FOSS photography, Windows 0-day, Xmonad"
image: images/thumbs/0314.jpg
date: Thu, 20 Dec 2018 19:25:31 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+21.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=uuI_SZV5J1s&amp;t=50s">0:50 SJW's at it again?  Debian removes package from repos due to name. 

<a href="https://www.youtube.com/watch?v=uuI_SZV5J1s&amp;t=594s">9:54 Ubuntu offered on two new Dell laptops. 

<a href="https://www.youtube.com/watch?v=uuI_SZV5J1s&amp;t=998s">16:38 Is it possible for professional photographers to use Linux and FOSS? 

<a href="https://www.youtube.com/watch?v=uuI_SZV5J1s&amp;t=998s">6:38 Is it possible for professional photographers to use Linux and FOSS? 

<a href="https://www.youtube.com/watch?v=uuI_SZV5J1s&amp;t=1460s">24:20 Windows zero-day lets you read any file with system level access. 

<a href="https://www.youtube.com/watch?v=uuI_SZV5J1s&amp;t=1695s">28:15 Big releases!   VirtualBox 6.0 and Linux Mint 19.1 "Tessa". 

<a href="https://www.youtube.com/watch?v=uuI_SZV5J1s&amp;t=1880s">31:20 While recording this video, I got this YT comment from Erik Dubois of ArcoLinux. 

REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.phoronix.com%2Fscan.php%3Fpage%3Dnews_item%26px%3DDebian-AH-Archive-Removal&amp;redir_token=GlByFcVf__004gZwAVtHbPSfJgx8MTU1MzU0MTk0NUAxNTUzNDU1NTQ1&amp;v=uuI_SZV5J1s&amp;event=video_description" target="_blank">https://www.phoronix.com/scan.php?pag...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fbugs.debian.org%2Fcgi-bin%2Fbugreport.cgi%3Fbug%3D907199%2347&amp;redir_token=GlByFcVf__004gZwAVtHbPSfJgx8MTU1MzU0MTk0NUAxNTUzNDU1NTQ1&amp;v=uuI_SZV5J1s&amp;event=video_description" target="_blank">https://bugs.debian.org/cgi-bin/bugre...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.reddit.com%2Fr%2Flinux%2Fcomments%2Fa7vlfu%2Fdebians_antiharassment_team_is_removing_a_package%2F&amp;redir_token=GlByFcVf__004gZwAVtHbPSfJgx8MTU1MzU0MTk0NUAxNTUzNDU1NTQ1&amp;v=uuI_SZV5J1s&amp;event=video_description" target="_blank">https://www.reddit.com/r/linux/commen...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fbetanews.com%2F2018%2F12%2F18%2Fubuntu-linux-18-04-lts-comes-to-dell-precision-5530-and-3530-mobile-workstations%2F&amp;redir_token=GlByFcVf__004gZwAVtHbPSfJgx8MTU1MzU0MTk0NUAxNTUzNDU1NTQ1&amp;v=uuI_SZV5J1s&amp;event=video_description" target="_blank">https://betanews.com/2018/12/18/ubunt...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.linuxjournal.com%2Fcontent%2Fphotography-and-linux&amp;redir_token=GlByFcVf__004gZwAVtHbPSfJgx8MTU1MzU0MTk0NUAxNTUzNDU1NTQ1&amp;v=uuI_SZV5J1s&amp;event=video_description" target="_blank">https://www.linuxjournal.com/content/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.bleepingcomputer.com%2Fnews%2Fsecurity%2Fwindows-zero-day-poc-lets-you-read-any-file-with-system-level-access%2F&amp;redir_token=GlByFcVf__004gZwAVtHbPSfJgx8MTU1MzU0MTk0NUAxNTUzNDU1NTQ1&amp;v=uuI_SZV5J1s&amp;event=video_description" target="_blank">https://www.bleepingcomputer.com/news...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Fwindows%2Fdesktop%2Fapi%2Fmsi%2Fnf-msi-msiadvertiseproducta&amp;redir_token=GlByFcVf__004gZwAVtHbPSfJgx8MTU1MzU0MTk0NUAxNTUzNDU1NTQ1&amp;v=uuI_SZV5J1s&amp;event=video_description" target="_blank">https://docs.microsoft.com/en-us/wind...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.virtualbox.org%2Fwiki%2FChangelog&amp;redir_token=GlByFcVf__004gZwAVtHbPSfJgx8MTU1MzU0MTk0NUAxNTUzNDU1NTQ1&amp;v=uuI_SZV5J1s&amp;event=video_description" target="_blank">https://www.virtualbox.org/wiki/Chang...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fblog.linuxmint.com%2F%3Fp%3D3669&amp;redir_token=GlByFcVf__004gZwAVtHbPSfJgx8MTU1MzU0MTk0NUAxNTUzNDU1NTQ1&amp;v=uuI_SZV5J1s&amp;event=video_description" target="_blank">https://blog.linuxmint.com/?p=3669

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2Farcolinuxd%2Farco-xmonad&amp;redir_token=GlByFcVf__004gZwAVtHbPSfJgx8MTU1MzU0MTk0NUAxNTUzNDU1NTQ1&amp;v=uuI_SZV5J1s&amp;event=video_description" target="_blank">https://github.com/arcolinuxd/arco-xm...
