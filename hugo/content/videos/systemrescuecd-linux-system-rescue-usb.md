---
title: "SystemRescueCd - A Linux System Rescue CD/DVD/USB"
image: images/thumbs/0108.jpg
date: Mon, 05 Feb 2018 01:44:50 +0000
author: Derek Taylor
tags: ["Distro Reviews", "SystemRescueCd"]
---

#### VIDEO

{{< amazon src="SystemRescueCd+-+A+Linux+System+Rescue+CDDVDUSB.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a quick look at a live Linux distro called SystemRescueCd which is a Gentoo-based distro intended to run as a bootable disk or USB stick, mainly to rescue Linux or Windows computers that have suffered some sort of crash. <a href="http://www.system-rescue-cd.org/">http://www.system-rescue-cd.org/</a>
