---
title: "Bluestar Linux First Impression Install & Review"
image: images/thumbs/0117.jpg
date: Mon, 19 Feb 2018 01:56:58 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Bluestar Linux", "KDE"]
---

#### VIDEO

{{< amazon src="Bluestar+Linux+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at Bluestar Linux--an Arch-based Linux distribution that features the KDE Plasma desktop environment. http://bluestarlinux.sourceforge.net/
