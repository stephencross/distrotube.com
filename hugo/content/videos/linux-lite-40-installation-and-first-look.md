---
title: "Linux Lite 4.0 Installation and First Look"
image: images/thumbs/0222.jpg
date: Fri, 01 Jun 2018 23:46:08 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Linux Lite"]
---

#### VIDEO

{{< amazon src="Linux+Lite+4.0+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Linux Lite 4.0 was just released. I run through the install process and take a quick first look at this Ubuntu-based distro that uses the XFCE desktop environment. https://www.linuxliteos.com/
