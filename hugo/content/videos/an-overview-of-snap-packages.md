---
title: "An Overview of Snap Packages"
image: images/thumbs/0382.jpg
date: Tue, 16 Apr 2019 02:53:16 +0000
author: Derek Taylor
tags: ["snap", ""]
---

#### VIDEO

{{< amazon src="An+Overview+of+Snap+Packages.mp4" >}}
&nbsp;

#### SHOW NOTES

Snap packages have become increasingly popular since they provide an  easy way to install software on any Linux distribution.  This a brief  overview of the Snap package format. NOTE TO ARCH USERS: When I installed snapd I meant to mention this.  If  If AppArmor isn't enabled in your system then all snaps will run in  devel mode which mean they will have same, unrestricted access to your  system as apps installed from Arch Linux repositories. 

$ systemctl enable --now apparmor.service 

$ systemctl enable --now snapd.apparmor.service 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=JRQaLyAesEg&amp;q=https%3A%2F%2Fsnapcraft.io%2F&amp;redir_token=YsOKpDVdJir4LmZxwKlNhVRp5rt8MTU1NzE5NzYwOEAxNTU3MTExMjA4" target="_blank">https://snapcraft.io/
