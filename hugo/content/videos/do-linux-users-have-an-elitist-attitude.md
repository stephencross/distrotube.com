---
title: "Do Linux Users Have An Elitist Attitude?"
image: images/thumbs/0564.jpg
date: 2020-03-07T12:22:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Do+Linux+Users+Have+An+Elitist+Attitude.mp4" >}}
&nbsp;

#### SHOW NOTES

It is often said that many, if not most, Linux users have an elitist attitude. We love to talk about how much better our OS is compared to the alternatives. Linux users often talk about how Linux changed their lives, changed the way they thought about computers and the world in general. And to those outside of the Linux community, we come across as a bunch or elitist, arrogant A-holes. But do we really have an elitist attitude? Yes, we do!

REFERENCED:
+ https://www.distrotube.com/blog/why-linux-users-have-elitist-attitude/
