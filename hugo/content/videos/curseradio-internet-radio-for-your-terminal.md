---
title: "Curseradio - Internet Radio For Your Terminal"
image: images/thumbs/0371.jpg
date: Sat, 23 Mar 2019 23:20:47 +0000
author: Derek Taylor
tags: ["TUI Apps", "terminal"]
---

#### VIDEO

{{< amazon src="Curseradio+Internet+Radio+For+Your+Terminal.mp4" >}}
&nbsp;

#### SHOW NOTES

Curseradio is a curses interface for browsing and playing an OPML  directory of internet radio streams. It is designed to use the tunein  directory found at <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=http%3A%2F%2Fopml.radiotime.com%2F&amp;redir_token=_7BTeDE_aRlAQqAwvJPlXY17MCx8MTU1MzY0MjYyNkAxNTUzNTU2MjI2&amp;event=video_description&amp;v=gIzwMpTDwVk" target="_blank">http://opml.radiotime.com/, but could be adapted to others. Audio playback uses mpv. Curseradio requires python3 and the libraries requests, xdg and lxml. 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2Fchronitis%2Fcurseradio&amp;redir_token=_7BTeDE_aRlAQqAwvJPlXY17MCx8MTU1MzY0MjYyNkAxNTUzNTU2MjI2&amp;event=video_description&amp;v=gIzwMpTDwVk" target="_blank">https://github.com/chronitis/curseradio
