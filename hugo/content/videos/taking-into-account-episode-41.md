---
title: "Taking Into Account, Ep. 41 - Break Up Facebook, Linux in Windows, Eoan Ermine, MS + Red Hat, Tilix"
image: images/thumbs/0390.jpg
date: Thu, 09 May 2019 14:20:30 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+41.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account:
+ 0:42 Facebook co-founder writes oped to NY Times calling for the breakup of Facebook.
+ 9:34 Beginning with Windows Insiders builds this Summer, Microsoft will ship a full Linux kernel in Win 10.
+ 12:59 Ubuntu 19.10 codename revealed. Is this the most obscure name for an Ubuntu release ever?
+ 16:09 MS CEO Satya Nadella comes to Red Hat Summit. Announces a new Microsoft/Red Hat partnership.
+ 19:24 The Tilix terminal emulator is looking for a new maintainer. Dev wants to focus on other things.
+ 24:18 You guys have been asking about how my new job might affect the channel.


+ 📰 REFERENCED:

+ https://www.nytimes.com/2019/05/09/opinion/sunday/chris-hughes-facebook-zuckerberg.html
+ https://www.theverge.com/2019/5/6/18534687/microsoft-windows-10-linux-kernel-feature
+ https://www.omgubuntu.co.uk/2019/04/ubuntu-19-10-codename
+ https://www.zdnet.com/article/microsoft-ceo-satya-nadella-comes-to-red-hat-summit/
+ https://github.com/gnunn1/tilix/issues/1700
+ https://www.omgubuntu.co.uk/2019/05/tilix-terminal-emulator-new-maintainer
