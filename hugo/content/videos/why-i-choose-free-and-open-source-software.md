---
title: "Why I Choose Free And Open Source Software"
image: images/thumbs/0391.jpg
date: Fri, 10 May 2019 14:24:26 +0000
author: Derek Taylor
tags: ["Public Service Announcements", ""]
---

#### VIDEO

{{< amazon src="Why+I+Choose+Free+And+Open+Source.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm often asked why I choose Linux over Windows and Mac. This is my answer.
&nbsp;

#### REFERENCED:
+ https://my.fsf.org/join
+ https://opensource.org/membership
