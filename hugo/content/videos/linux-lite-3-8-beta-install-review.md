---
title: "Linux Lite 3.8 Beta Install & Review"
image: images/thumbs/0081.jpg
date: Thu, 04 Jan 2018 01:06:28 +0000
author: Derek Taylor
tags: ["Linux Lite", "XFCE"]
---

#### VIDEO

{{< amazon src="Linux+Lite+3.8+Beta+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a quick look at the recently released beta version of Linux Lite. It is a distro based on Ubuntu's LTS and featuring the XFCE desktop environment. Linux Lite is fast and lightweight. <a href="https://linuxliteos.com/">https://linuxliteos.com/</a>
