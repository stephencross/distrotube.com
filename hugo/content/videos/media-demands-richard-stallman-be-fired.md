---
title: "Media Demands Richard Stallman Be Fired"
image: images/thumbs/0441.jpg
date: Sun, 15 Sep 2019 23:44:00 +0000
author: Derek Taylor
tags: ["Richard Stallman", ""]
---

#### VIDEO

{{< amazon src="Media+Demands+Richard+Stallman+Be+Fired.mp4" >}}
&nbsp;

#### SHOW NOTES

I tried to warn you guys. Now it's here. Mainstream media outlets are demanding Richard Stallman be fired for his recent comments defending men accused of sexually assaulting underage girls involved with the Jeffrey Epstein sex trafficking ring.


REFERENCED:
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fmedium.com%2F%40selamie%2Fremove-richard-stallman-fec6ec210794&amp;v=g2s5hPLUHN8&amp;event=video_description&amp;redir_token=vBLK_fdggA0cV9WL2ZZdHsxl4BV8MTU3NzQ5MDI5OEAxNTc3NDAzODk4" target="_blank" rel="nofollow noopener noreferrer">https://medium.com/@selamie/remove-ri...
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.vice.com%2Fen_us%2Farticle%2F9ke3ke%2Ffamed-computer-scientist-richard-stallman-described-epstein-victims-as-entirely-willing&amp;v=g2s5hPLUHN8&amp;event=video_description&amp;redir_token=vBLK_fdggA0cV9WL2ZZdHsxl4BV8MTU3NzQ5MDI5OEAxNTc3NDAzODk4" target="_blank" rel="nofollow noopener noreferrer">https://www.vice.com/en_us/article/9k...
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.thedailybeast.com%2Ffamed-mit-computer-scientist-richard-stallman-defends-epstein-victims-were-entirely-willing&amp;v=g2s5hPLUHN8&amp;event=video_description&amp;redir_token=vBLK_fdggA0cV9WL2ZZdHsxl4BV8MTU3NzQ5MDI5OEAxNTc3NDAzODk4" target="_blank" rel="nofollow noopener noreferrer">https://www.thedailybeast.com/famed-m...
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.dailykos.com%2Fstories%2F2019%2F9%2F13%2F1885337%2F-Richard-Stallman-Needs-to-be-Fired&amp;v=g2s5hPLUHN8&amp;event=video_description&amp;redir_token=vBLK_fdggA0cV9WL2ZZdHsxl4BV8MTU3NzQ5MDI5OEAxNTc3NDAzODk4" target="_blank" rel="nofollow noopener noreferrer">https://www.dailykos.com/stories/2019...
