---
title: "Stress Terminal UI (s-tui) - Stress-Testing System Monitor"
image: images/thumbs/0046.jpg
date: Fri, 01 Dec 2017 14:35:07 +0000
author: Derek Taylor
tags: ["TUI Apps", "s-tui"]
---

#### VIDEO

{{< amazon src="Stress+Terminal+UI+(s-tui)+-+Stress-Testing+System+Monitor.mp4" >}}  
&nbsp;

#### SHOW NOTES

S-tui is a terminal-based application for monitoring your system. S-tui monitors CPU temperature, frequency, power and utilization in a graphical way from the terminal. <a href="https://github.com/amanusk/s-tui">https://github.com/amanusk/s-tui</a>
