---
title: "The Pacman Package Manager in Arch Linux"
image: images/thumbs/0162.jpg
date: Thu, 29 Mar 2018 22:11:47 +0000
author: Derek Taylor
tags: ["Package Managers", ""]
---

#### VIDEO

{{< amazon src="The+Pacman+Package+Manager+in+Arch+Linux.mp4" >}}  
&nbsp;

#### SHOW NOTES

A quick overview of the pacman package manager utility in Arch Linux and most Arch-based distros. I show you a few of the most often used commands you might need. For more information on pacman, check the manpage and check the Arch Wiki: <a href="https://wiki.archlinux.org/index.php/pacman">https://wiki.archlinux.org/index.php/pacman</a>
