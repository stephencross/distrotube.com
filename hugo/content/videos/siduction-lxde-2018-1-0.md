---
title: "Siduction LXDE 2018.1.0 Install & Review"
image: images/thumbs/0079.jpg
date: Fri, 05 Jan 2018 01:01:14 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Siduction"]
---

#### VIDEO

{{< amazon src="Siduction+LXDE+2018.1.0+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today I take a quick look at Siduction's LXDE edition. Siduction is a rolling release model Linux distro based on Debian's unstable branch. <a href="https://forum.siduction.org/">https://forum.siduction.org/</a>
