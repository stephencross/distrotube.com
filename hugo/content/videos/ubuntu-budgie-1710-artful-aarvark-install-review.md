---
title: "Ubuntu Budgie 17.10 Artful Aarvark - Install and Review"
image: images/thumbs/0018.jpg
date: Fri, 20 Oct 2017 02:04:03 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu Budgie", "Budgie"]
---

#### VIDEO

{{< amazon src="Ubuntu+Budgie+17.10+Artful+Aarvark+-+Install+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today I review the recent release of Ubuntu Budgie 17.10 "Artful Aadvark." This is my first time installing and using Ubuntu's Budgie edition, and it is also my first experience with the Budgie desktop environment.
