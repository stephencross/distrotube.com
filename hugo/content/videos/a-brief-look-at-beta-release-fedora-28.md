---
title: "A Brief Look at the Beta Release of Fedora 28"
image: images/thumbs/0167.jpg
date: Tue, 03 Apr 2018 22:18:05 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Fedora"]
---

#### VIDEO

{{< amazon src="A+Brief+Look+at+the+Beta+Release+of+Fedora+28.mp4" >}}
&nbsp;

#### SHOW NOTES

I take a very brief look at the recently released beta of Fedora 28. The official release of Fedora 28 is not due out for another month (assuming it isn't delayed) but I wanted to see how the beta was shaping up.<a href="https://fedoramagazine.org/announcing-fedora-28-beta/">https://fedoramagazine.org/announcing-fedora-28-beta/</a>
