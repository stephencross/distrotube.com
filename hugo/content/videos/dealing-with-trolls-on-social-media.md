---
title: "Dealing With Trolls On Social Media"
image: images/thumbs/0395.jpg
date: Fri, 17 May 2019 14:37:02 +0000
author: Derek Taylor
tags: ["Social Media", ""]
---

#### VIDEO

{{< amazon src="Dealing+With+Trolls+On+Social+Media.mp4" >}}
&nbsp;

#### SHOW NOTES

Just a few quick thoughts on trolls while driving to the gym. We all have to deal with trolls on the Internet. What drives a troll? What makes the troll tick? How do you combat a troll?
NOTE: Patrons were granted early access (one week) to this video. I want to thank my subscribers over on Patreon. You guys rock! 
+ Patreon: https://www.patreon.com/distrotube
