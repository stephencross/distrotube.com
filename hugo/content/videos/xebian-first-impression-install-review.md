---
title: "Xebian First Impression Install & Review"
image: images/thumbs/0044.jpg
date: Thu, 30 Nov 2017 14:31:44 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Xebian", "XFCE"]
---

#### VIDEO

{{< amazon src="Xebian+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today I'm taking a look at Xebian, a rolling release Linux distro based on Debian Sid and using the XFCE desktop environment. This is a new distro that few people will have heard of. It's not even listed on Distrowatch.com yet, though it is on their waiting list to be reviewed. <a href="https://xebian.org/">https://xebian.org/</a>
