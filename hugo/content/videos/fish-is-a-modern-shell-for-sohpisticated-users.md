---
title: "Fish Is A Modern Shell For The Sophisticated User"
image: images/thumbs/0592.jpg
date: 2020-04-09T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Fish+Is+A+Modern+Shell+For+The+Sophisticated+User.mp4" >}}
&nbsp;

#### SHOW NOTES

Fish is a user-friendly command line shell for Linux and MacOS. It has a number of smart features and improvements when compared to other popular shells, namely Bash and Zsh.  

REFERENCED:
+ https://fishshell.com/ - Fish Website
+ https://github.com/oh-my-fish/oh-my-fish - Oh My Fish