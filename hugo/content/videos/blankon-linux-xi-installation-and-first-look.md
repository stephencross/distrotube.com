---
title: "BlankOn Linux XI Installation and First Look"
image: images/thumbs/0258.jpg
date: Mon, 20 Aug 2018 22:40:28 +0000
author: Derek Taylor
tags: ["Distro Reviews", "BlankOn"]
---

#### VIDEO

{{< amazon src="BlankOn+Linux+XI+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm taking a quick look at BlankOn Linux, an Indonesian distro that is based on Debian. Never looked at this one before so this is very much a first impression video. https://www.blankonlinux.or.id/
