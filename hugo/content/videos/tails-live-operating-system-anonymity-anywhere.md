---
title: "Tails Live Operating System - Privacy For Anyone Anywhere"
image: images/thumbs/0023.jpg
date: Sat, 28 Oct 2017 02:26:31 +0000
author: Derek Taylor
tags: ["Distro Reviews", "TAILS"]
---

#### VIDEO

{{< amazon src="Tails+Live+Operating+System+-+Privacy+For+Anyone+Anywhere.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a look at a very different kind of Linux distribution in this review. Tails (The Amnesic Incognito Live System) is a live operating system designed to be run off a thumb drive or a DVD. It's purpose is to provide privacy and anonymity to the user. Tails - https://tails.boum.org/ Tor - https://www.torproject.org/
