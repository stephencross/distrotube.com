---
title: "Org Mode Basics In Doom Emacs"
image: images/thumbs/0553.jpg
date: 2020-02-24T12:22:40+06:00
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

{{< amazon src="org-mode-basics.mp4" >}}
&nbsp;

#### SHOW NOTES

Org Mode is a document editing, formatting, and organizing mode, designed for notes, planning, and outlining within Doom Emacs.  This video demonstrates some of the basic functions of Org Mode.

REFERENCED:
+ https://github.com/hlissner/doom-emacs - Doom Emacs