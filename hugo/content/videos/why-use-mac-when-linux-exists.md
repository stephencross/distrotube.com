---
title: "Why Use Mac When Linux Exists?"
image: images/thumbs/0305.jpg
date: Sat, 10 Nov 2018 00:18:42 +0000
author: Derek Taylor
tags: ["Public Service Announcements", ""]
---

#### VIDEO

{{< amazon src="Why+Use+Mac+When+Linux+Exists.mp4" >}}
&nbsp;

#### SHOW NOTES

Are you a Mac user?  Have you heard of Linux?  If so, what are waiting for?  Make the switch today!

MUSIC:
"Shattered Paths" by Aakash Gandhi ( <a href="https://www.youtube.com/user/88keystoeuphoria/videos">https://www.youtube.com/user/88keysto... )
The track can be found in the YouTube Audio Library.
