---
title: "A Quick Look At Zoonity OS Britannia - DT LIVE"
image: images/thumbs/0262.jpg
date: Sat, 04 Aug 2018 19:22:29 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Zoonity OS", "Live Stream"]
---

#### VIDEO

{{< amazon src="A+Quick+Look+At+Zoonity+OS+Britannia+-+DT+LIVE.mp4" >}}
&nbsp;

#### SHOW NOTES

In this impromptu live stream, I'm going to take a quick first look at Zoonity OS Britannia.  It is a Xubuntu-based distro that sports the Xfce desktop but made to look like Unity.  Will it be awesome or just another distro? 
