---
title: "Nextcloud Hub Competes With Office 365 And Google Docs"
image: images/thumbs/0530.jpg
date: 2020-01-30T12:22:40+06:00
author: Derek Taylor
tags: ["Nextcloud", "App Reviews"]
---

#### VIDEO

{{< amazon src="Nextcloud+Hub+Competes+With+Office+365+And+Google+Docs.mp4" >}}
&nbsp;

#### SHOW NOTES

The latest version of Nextcloud marked a new direction for the project, including a new name--Nextcloud Hub!  No longer is Nextcloud just a file syncing app.  Now you have a calendar, contact list, email client, video chat client and groupware file editing functions available.  Nextcloud Hub is setting up to be a nice free and open source alternative to Office 365 and Google Docs.

REFERENCED:
+ https://nextcloud.com/hub/