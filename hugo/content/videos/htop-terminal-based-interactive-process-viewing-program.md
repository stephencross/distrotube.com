---
title: "Htop - Terminal-Based Interactive Process Viewing Program"
image: images/thumbs/0037.jpg
date: Mon, 20 Nov 2017 14:19:06 +0000
author: Derek Taylor
tags: ["TUI Apps", "htop"]
---

#### VIDEO

{{< amazon src="Htop+-+Terminal-Based+Interactive+Process+Viewing+Program.mp4" >}}  
&nbsp;

#### SHOW NOTES

A quick look at htop -- a better "top" program. <a href="http://hisham.hm/htop/">http://hisham.hm/htop/</a>
