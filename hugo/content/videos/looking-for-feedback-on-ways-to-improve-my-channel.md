---
title: "Looking For Feedback On Ways To Improve My Channel"
image: images/thumbs/0470.jpg
date: Tue, 26 Nov 2019 00:20:00 +0000
author: Derek Taylor
tags: ["", "", ""]
---

#### VIDEO

{{< amazon src="Looking+For+Feedback+On+Ways+To+Improve+My+Channel.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm re-evaluating things on my YouTube channel and am seeking feedback from the community on things that I could improve on. So, what is it you guys would like to see from me as a YouTuber? Different content? Different styles of video? Constructive criticism is appreciated. I look forward to your responses. You guys are the best!
