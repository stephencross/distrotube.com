---
title: "Day 25 of Tiling Window Managers and Terminal Apps"
image: images/thumbs/0197.jpg
date: Sat, 05 May 2018 22:59:36 +0000
author: Derek Taylor
tags: ["Tiling Window Managers", "terminal"]
---

#### VIDEO

{{< amazon src="Day+25+of+Tiling+Window+Managers+and+Terminal+Apps.mp4" >}}
&nbsp;

#### SHOW NOTES

Day 25 of my 30 challenge. I'm living in a tiling window manager for 30 days and only using terminal-based applications where possible. Just a quick update on how it's going. 
