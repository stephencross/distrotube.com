---
title: "Reddio - Reddit From The Command Line"
image: images/thumbs/0439.jpg
date: Thu, 22 Aug 2019 23:40:00 +0000
author: Derek Taylor
tags: ["command line", "terminal", "Social Media"]
---

#### VIDEO

{{< amazon src="Command+Line+Reddit+Viewer+Reddio.mp4" >}}
&nbsp;

#### SHOW NOTES

Today I am going to share with you a really neat Reddit command line application called Reddio. View and post from the command line. Use Reddio in your shell scripts, or use it in conjunction with other command line utilities, or integrate it with your favorite run launcher (such as dmenu).


&nbsp;
#### REFERENCED: 
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=aLLY6C0Wvyc&amp;event=video_description&amp;q=https%3A%2F%2Fgitlab.com%2FaaronNG%2Freddio&amp;redir_token=-fzmMQFlNBktuySazWg1vnMfU-Z8MTU3NzQ4OTI5MkAxNTc3NDAyODky" target="_blank" rel="nofollow noopener noreferrer">https://gitlab.com/aaronNG/reddio
