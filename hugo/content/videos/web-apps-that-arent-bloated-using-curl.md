---
title: "Web Apps That Aren't Bloated! Using Curl."
image: images/thumbs/0411.jpg
date: Sat, 08 Jun 2019 03:37:58 +0000
author: Derek Taylor
tags: ["terminal", "command line", "curl"]
---

#### VIDEO

{{< amazon src="Web+Apps+That+Arent+Bloated+Using+Curl.mp4" >}}
&nbsp;

#### SHOW NOTES

Some useful things you can do with the curl command line utility. And some fun stuff you can do with it, as well!


&nbsp;
#### REFERENCED:
+ GET THE WEATHER
+ curl wttr.in
+ curl wttr.in/Berlin
+ GET YOUR IP ADDRESS
+ curl ifconfig.co
+ GET YOUR LOCATION
+ curl ifconfig.co/country
+ curl ifconfig.co/city
+ TINYURL URL SHORTENER
+ curl -s http://tinyurl.com/api-create.php?url...
+ GETNEWS.TECH
+ curl getnews.tech
+ curl getnews.tech/trump
+ curl getnews.tech/nba+finals
+ CHEAT SHEETS
+ curl cheat.sh/btrfs
+ CRYPTOCURRENCY EXCHANGE RATES
+ curl rate.sx
+ DICTIONARY
+ curl 'dict://dict.org/d:operating system'
+ FUN WITH PARROTS
+ curl parrot.live
+ (also checkout parrotsay: https://github.com/matheuss/parrotsay )
+ FUN WITH RICK ASTLEY
+ https://github.com/keroserene/rickrollrc
