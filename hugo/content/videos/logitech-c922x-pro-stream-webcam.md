---
title: "Logitech c922x Pro Stream Webcam"
image: images/thumbs/0143.jpg
date: Wed, 14 Mar 2018 21:34:32 +0000
author: Derek Taylor
tags: ["Hardware", ""]
---

#### VIDEO

{{< amazon src="Logitech+c922x+Pro+Stream+Webcam.mp4" >}}  
&nbsp;

#### SHOW NOTES

A very quick video of me plugging in my new Logitech c922x Pro Stream Webcam and recording through it for the first time in Guvcview. Pretty excited about it so I thought I'd share. 
