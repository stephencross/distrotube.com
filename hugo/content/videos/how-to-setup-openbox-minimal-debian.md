---
title: "How to setup Openbox on a minimal install of Debian"
image: images/thumbs/0083.jpg
date: Sun, 07 Jan 2018 01:09:38 +0000
author: Derek Taylor
tags: ["Debian", "Openbox"]
---

#### VIDEO

{{< amazon src="How+to+setup+Openbox+on+a+minimal+install+of+Debian.mp4" >}}
&nbsp;

#### SHOW NOTES

In this rather lengthy video, I run through how to get a basic openbox session up and running on top of a minimal install of Debian. I show the programs that I usually install, how to install them from the command line, how to setup an autostart file for openbox, how to edit the tint2 panel, etc. Hope you guys enjoy! 

Referenced in the video: <a href="https://www.debian.org/CD/netinst/">https://www.debian.org/CD/netinst/ </a><a href="https://gitlab.com/dwt1">https://gitlab.com/dwt1 </a>    In this video, I installed as root (su): apt install openbox obconf obmenu lightdm pcmanfm iceweasel leafpad lxterminal nitrogen tint2 menu lxappearance compton synaptic gnome-backgrounds mate-backgrounds
