---
title: "MagpieOS 2.2 GNOME Install and Review"
image: images/thumbs/0185.jpg
date: Mon, 23 Apr 2018 22:45:24 +0000
author: Derek Taylor
tags: ["Distro Reviews", "MagpieOS", "GNOME"]
---

#### VIDEO

{{< amazon src="MagpieOS+2.2+GNOME+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I do a quick install and overview of MagpieOS 2.2 GNOME. This is an Arch-based Linux distro that offers two different dekstop versions--GNOME and XFCE. Today, I look at their GNOME edition. http://magpieos.net/
