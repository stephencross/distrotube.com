---
title: "Live Feb 11, 2018 - Obscure Window Manager Project - Openbox"
image: images/thumbs/0112.jpg
date: Sun, 11 Feb 2018 01:51:09 +0000
author: Derek Taylor
tags: ["Openbox", "Live Stream"]
---

#### VIDEO

{{< amazon src="Live+Feb+11%2C+2018+-+Obscure+Window+Manager+Project+-+Openbox.mp4" >}}  
&nbsp;

#### SHOW NOTES

Time to review the third window manager in the group of 12 "obscure" window managers that I am reviewing as part of the Obscure Window Manager Project. Since this window manager is Openbox (my personal favorite), I figured "Why not do this one live?!" 
