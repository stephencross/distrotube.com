---
title: "Live Mar 14, 2018 - Counter Strike: Global Offensive"
image: images/thumbs/0150.jpg
date: Wed, 14 Mar 2018 21:42:31 +0000
author: Derek Taylor
tags: ["Live Stream", "Gaming", "CSGO"]
---

#### VIDEO

{{< amazon src="Live+Mar+14%2C+2018+-+Counter+Strike+-+Global+Offensive-x74j4oTiVRA.mp4" >}}
&nbsp;

#### SHOW NOTES

In this spur-of-the-moment live stream, I give Counter Strike: Global Offensive another shot.  In a previous video I did of me attempting CSGO, I looked pretty bad playing the game.  I figured I'd try it again except this time LIVE!
