---
title: "The Vim Tutorial - Part One - Basic Commands"
image: images/thumbs/0317.jpg
date: Tue, 11 Dec 2018 19:31:43 +0000
author: Derek Taylor
tags: ["Vim", ""]
---

#### VIDEO

{{< amazon src="The+Vim+Tutorial+-+Part+One+-+Basic+Commands.mp4" >}}
&nbsp;

#### SHOW NOTES

Let's get started with Vim!  This video covers a few basic commands.   But this is just scratching the surface of Vim.  There is much more to  Vim (to be covered in future videos). 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=ER5JYFKkYDg&amp;event=video_description&amp;redir_token=M4s5mjKPg4AoG25Ee2oVmIod4Fx8MTU1MzU0MjMwN0AxNTUzNDU1OTA3&amp;q=https%3A%2F%2Fwww.vim.org%2F" target="_blank">https://www.vim.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=ER5JYFKkYDg&amp;event=video_description&amp;redir_token=M4s5mjKPg4AoG25Ee2oVmIod4Fx8MTU1MzU0MjMwN0AxNTUzNDU1OTA3&amp;q=https%3A%2F%2Fvim-adventures.com%2F" target="_blank">https://vim-adventures.com/
