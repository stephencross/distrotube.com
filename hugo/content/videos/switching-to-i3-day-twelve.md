---
title: "Switching to i3 - Day 12 - Binding Modes"
image: images/thumbs/0334.jpg
date: Sat, 12 Jan 2019 20:11:31 +0000
author: Derek Taylor
tags: ["tiling window managers", "i3wm"]
---

#### VIDEO

{{< amazon src="Switching+to+i3+Day+12+Binding+Modes.mp4" >}}
&nbsp;

#### SHOW NOTES

It's been 12 days since I switched to i3 (thanks to you guys voting for it). How am I doing? Just fine. One of the neat things I've learned about i3 is "binding modes." I will discuss this as well as a couple of minor issues I am having with i3.
