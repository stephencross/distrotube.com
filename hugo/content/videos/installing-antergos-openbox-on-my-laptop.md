---
title: "Installing Antergos Openbox On My Laptop"
image: images/thumbs/0232.jpg
date: Sun, 17 Jun 2018 00:08:45 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Antergos"]
---

#### VIDEO

{{< amazon src="Installing+Antergos+Openbox+On+My+Laptop.mp4" >}}
&nbsp;

#### SHOW NOTES

I've got that urge...the urge to distro-hop! Putting Antergos Openbox on my laptop. https://antergos.com/
