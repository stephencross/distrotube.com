---
title: "Bliss, An Open Source Android-Based OS That Runs On A PC"
image: images/thumbs/0254.jpg
date: Wed, 08 Aug 2018 22:32:21 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Bliss"]
---

#### VIDEO

{{< amazon src="Bliss%2C+An+Open+Source+Android-Based+OS+That+Runs+On+A+PC.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm taking a quick first look at Bliss, an Android-based OS that claims to run on phones, tablets, laptops and PCs. Today, I'm installing it in a VM. Thanks, Jesper, for letting me know about Bliss! https://blissroms.com/
