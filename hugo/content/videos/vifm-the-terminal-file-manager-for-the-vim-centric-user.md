---
title: "vifm - The Terminal File Manager For The Vim-Centric User"
image: images/thumbs/0322.jpg
date: Tue, 25 Dec 2018 19:39:07 +0000
author: Derek Taylor
tags: ["terminal", "Vim", "vifm"]
---

#### VIDEO

{{< amazon src="vifm+The+Terminal+File+Manager+For+The+Vim+Centric+User.mp4" >}}
&nbsp;

#### SHOW NOTES

About three weeks ago, I did a video tutorial on Vim.  After that video,
 I was looking around for other terminal applications that use Vim 
keybindings and Vim syntax.  I found vifm--a dual-paned terminal file 
manager that functions very much like Vim.

<a href="https://www.youtube.com/redirect?q=https%3A%2F%2Fvifm.info%2F&amp;v=47QYCa8AYG4&amp;redir_token=eF5wcHu6lNk3bY1VF-IplaNvUVd8MTU1MzU0Mjc4NUAxNTUzNDU2Mzg1&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://vifm.info/
