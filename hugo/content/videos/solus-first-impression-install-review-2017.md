---
title: "Solus First Impression Install & Review"
image: images/thumbs/0050.jpg
date: Wed, 06 Dec 2017 14:41:05 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Solus", "Budgie"]
---

#### VIDEO

{{< amazon src="Solus+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today I take a look at an independent Linux distro called Solus. It is a rolling release distro that sports a custom desktop environment (Budgie) that is functional and gorgeous. <a href="https://solus-project.com/">https://solus-project.com/</a>
