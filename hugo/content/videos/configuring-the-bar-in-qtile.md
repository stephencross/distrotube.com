---
title: "Configuring The Bar In Qtile"
image: images/thumbs/0518.jpg
date: 2020-01-18T12:22:40+06:00
author: Derek Taylor
tags: ["Tiling Window Managers", "Qtile"]
---

#### VIDEO

{{< amazon src="Configuring+The+Bar+In+Qtile.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I will show you have to configure the bar (aka. the panel) in qtile.  There are dozens of builtin widgets that you can use in qtile.  The bar is highly configurable and can be made quite attractive.

REFERENCED:
+ http://www.qtile.org/
