---
title: "Listen To Podcasts In The Terminal With Castero"
image: images/thumbs/0459.jpg
date: Wed, 06 Nov 2019 00:11:00 +0000
author: Derek Taylor
tags: ["terminal", "TUI Apps"]
---

#### VIDEO

{{< amazon src="Listen+To+Podcasts+In+The+Terminal+With+Castero.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I'm going to discuss a neat terminal-based podcast client called Castero. It is written in Python and licensed under the MIT license. Light and minimal with just enough functionality built in to it.

#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=OGQ0_fza9CQ&amp;event=video_description&amp;q=https%3A%2F%2Fgithub.com%2Fxgi%2Fcastero&amp;redir_token=CJLzT806kKxJ9GQXSsS273MJ8Lt8MTU3NzQ5MTkyNkAxNTc3NDA1NTI2" target="_blank" rel="nofollow noopener noreferrer">https://github.com/xgi/castero
