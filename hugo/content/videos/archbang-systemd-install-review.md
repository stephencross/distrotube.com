---
title: "Archbang (systemd) Install & Review"
image: images/thumbs/0071.jpg
date: Sat, 22 Dec 2017 15:52:26 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Archbang+(systemd)+Install+%26+Review.mp4?_=1" >}}  
&nbsp;

#### SHOW NOTES

Today I install and review Archbang (systemd).  It is an Arch-based Linux distro that uses the Openbox window manager, tint2 panel and conky system monitor.  It is fast and lightweight. http://bbs.archbang.org/
