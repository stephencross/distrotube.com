---
title: "BunsenLabs Linux Install & Review"
image: images/thumbs/0041.jpg
date: Sun, 26 Nov 2017 14:26:00 +0000
author: Derek Taylor
tags: ["Distro Reviews", "BunsenLabs", "Openbox"]
---

#### VIDEO

{{< amazon src="BunsenLabs+Linux+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video I take a quick look at BunsenLabs, a Debian-based Linux distro that uses the Openbox window manager, tint2 panel, and the conky system monitor.
