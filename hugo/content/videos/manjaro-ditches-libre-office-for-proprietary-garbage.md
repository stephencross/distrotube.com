---
title: "Manjaro Ditches LibreOffice For Proprietary Garbage"
image: images/thumbs/0427.jpg
date: Thu, 01 Aug 2019 23:11:00 +0000
author: Derek Taylor
tags: ["Manjaro", "Proprietary Software"]
---

#### VIDEO

{{< amazon src="Manjaro+Ditches+LibreOffice+For+Proprietary+Garbage.mp4" >}}
&nbsp;

#### SHOW NOTES

Rant incoming! Manjaro has made the decision to no longer ship with LibreOffice on it's ISOs in favor of a proprietary office suite called FreeOffice. The Manjaro community is not happy about this decision and neither am I.


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=_bkn0wQp1Yw&amp;q=https%3A%2F%2Fforum.manjaro.org%2Ft%2Ftesting-update-2019-07-29-kernels-xfce-4-14-pre3-haskell%2F96690&amp;redir_token=pQ9enuSkTF1hx8yjrhb07Q2NfW58MTU3NzQ4ODI4NEAxNTc3NDAxODg0" target="_blank" rel="nofollow noopener noreferrer">https://forum.manjaro.org/t/testing-u...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=_bkn0wQp1Yw&amp;q=https%3A%2F%2Fforum.manjaro.org%2Ft%2Fincluding-freeoffice-is-a-bad-idea-was-manjaro-roadmap%2F96754&amp;redir_token=pQ9enuSkTF1hx8yjrhb07Q2NfW58MTU3NzQ4ODI4NEAxNTc3NDAxODg0" target="_blank" rel="nofollow noopener noreferrer">https://forum.manjaro.org/t/including...
