---
title: "Antergos GNOME Install & Review"
image: images/thumbs/0116.jpg
date: Sun, 18 Feb 2018 01:56:02 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Antergos", "GNOME"]
---

#### VIDEO

{{< amazon src="Antergos+GNOME+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I check out the GNOME edition of Antergos--a popular Arch-based Linux distribution that has a substantial following.
