---
title: "Chown, Chmod and Shell Aliases"
image: images/thumbs/0515.jpg
date: 2020-01-15T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Chown%2C+Chmod+and+Shell+Aliases.mp4" >}}
&nbsp;

#### SHOW NOTES

I will give a brief overview of Linux file ownership and permissions with the chown and chmod commands. 

REFERENCED:
+ https://linux.die.net/man/1/chown
+ https://linux.die.net/man/1/chmod
