---
title: "ArcoLinux Xmonad - A Quick First Look"
image: images/thumbs/0338.jpg
date: Sat, 19 Jan 2019 22:33:30 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ArcoLinux", "Xmonad"]
---

#### VIDEO

{{< amazon src="ArcoLinux+Xmonad+A+Quick+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today I'm taking a quick first look at ArcoLinux Xmonad.  I installed it
 twice, once in a virtual machine and again on physical hardware.   I 
install it in a VM to record the installation process on camera.  Then I
 record a bit of me playing around with the physical install on my 
Lenovo Thinkpad laptop. 

<a href="https://www.youtube.com/redirect?redir_token=_JZq--h6i1mA3KHOMhHVfqqhc-98MTU1MzYzOTYyOUAxNTUzNTUzMjI5&amp;q=https%3A%2F%2Farcolinux.info%2F&amp;v=XMeWnzTzhds&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://arcolinux.info/
