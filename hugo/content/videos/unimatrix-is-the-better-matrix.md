---
title: "Unimatrix Is The Better Matrix"
image: images/thumbs/0408.jpg
date: Sat, 01 Jun 2019 03:31:49 +0000
author: Derek Taylor
tags: ["terminal", "TUI Apps"]
---

#### VIDEO

{{< amazon src="Unimatrix+Is+The+Better+Matrix.mp4" >}}
&nbsp;

#### SHOW NOTES

Unimatrix is a Python script to simulate the display from "The Matrix" in terminal. It is based on cmatrix but includes some added functionality that make it a worthy successor to cmatrix.


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=3WPN317nHX7QzlPzdLG0iEbQ5v58MTU2NjUzMTExMkAxNTY2NDQ0NzEy&amp;event=video_description&amp;v=TkZAa0uoFE8&amp;q=https%3A%2F%2Fgithub.com%2Fwill8211%2Funimatrix" target="_blank" rel="nofollow noopener noreferrer">https://github.com/will8211/unimatrix
