---
title: "DeaDBeeF Is A Lightweight and Modular Music Player"
image: images/thumbs/0531.jpg
date: 2020-01-31T12:22:40+06:00
author: Derek Taylor
tags: ["Linux Audio", ""]
---

#### VIDEO

{{< amazon src="DeaDBeeF+Is+A+Lightweight+and+Modular+Music+Player.mp4" >}}
&nbsp;

#### SHOW NOTES

DeaDBeeF has been my preferred GUI music player for many years now.  Despite the funny name, it is quite a powerful little player.  It is available on Linux, BSD, macOS and other Unix-like operating systems.  It is modular in nature and allows for the creation and installation of third-party plugins.

REFERENCED:
+ https://deadbeef.sourceforge.io/