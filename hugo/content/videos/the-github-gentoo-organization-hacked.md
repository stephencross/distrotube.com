---
title: "The Github Gentoo Organization Hacked!"
image: images/thumbs/0237.jpg
date: Fri, 29 Jun 2018 00:14:27 +0000
author: Derek Taylor
tags: ["Gentoo", "Security"]
---

#### VIDEO

{{< amazon src="The+Github+Gentoo+Organization+Hacked!.mp4" >}}
&nbsp;

#### SHOW NOTES

Gentoo has been hacked! Or at least, there Github repos and the pages hosted there have been compromised.
