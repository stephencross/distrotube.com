---
title: "Antergos Cinnamon First Impression Install & Review"
image: images/thumbs/0006.jpg
date: Tue, 10 Oct 2017 01:44:27 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Antergos", "Cinnamon"]
---

#### VIDEO

{{< amazon src="Antergos+Cinnamon+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today I'm installing Antergos Linux for the first time. Going to run through the install and give my first impression on the installer and the Antergos Cinnamon edition. I will keep this installation updated and will revisit in the coming weeks and months ahead to see how things progress.
