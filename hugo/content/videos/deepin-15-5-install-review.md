---
title: "Deepin 15.5 First Impression Install & Review"
image: images/thumbs/0047.jpg
date: Fri, 01 Dec 2017 14:36:35 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Deepin"]
---

#### VIDEO

{{< amazon src="Deepin+15.5+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I'm going to install and review Deepin 15.5, a Debian-based Linux distro that uses it's own custom desktop environment. Based out of China but aimed at being a global desktop operating system, Deepin is one of the most interesting and refreshing Linux distros I've had the pleasure to review. <a href="https://www.deepin.org">https://www.deepin.org</a>
