---
title: "Suckless Tabbed Adds Tabs To Any Program"
image: images/thumbs/0580.jpg
date: 2020-03-27T12:22:40+06:00
author: Derek Taylor
tags: ["Suckless", ""]
---

#### VIDEO

{{< amazon src="Suckless+Tabbed+Adds+Tabs+To+Any+Program.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I will briefly introduce "tabbed" which is a Suckless utility that adds tabs to programs that lack builtin tabbing, such as terminals (like st and urxvt) and the Suckless surf browser.

REFERENCED:
+ https://tools.suckless.org/tabbed/
