---
title: "Contact"
date: 2018-07-07T15:53:27+06:00
draft: false
---

### Contact Information

You can get in touch with DT on the following platforms.

+ [Patreon](https://www.patreon.com/distrotube)
+ [Mastodon](https://mastodon.technology/@distrotube)
+ [Reddit](https://www.reddit.com/r/distrotube/)
+ [YouTube](https://www.youtube.com/DistroTube)

You may also contact DT on the forums (if you are member) on this website.
