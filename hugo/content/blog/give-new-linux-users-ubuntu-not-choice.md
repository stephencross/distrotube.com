---
title: "Give New Linux Users Ubuntu, Not Choice"
date: 2020-03-28T12:22:40+06:00
image: images/blog/0014.jpg
author: Derek Taylor
---

### What should we present to new Linux users?

So I see this has been a hot topic in the Linux community lately.  And that topic is choice.  And specifically the choices that we present to would-be New Linux Users.  And the reason this topic has been discussed so much lately is because of an interview that Jason Evangelho of the [Linux4Everyone](https://www.gitlab.com/dwt1/) podcast did with Alan Pope (aka. Popey).  Jason asked Popey the following question: "Do you think there is anything that needs to happen to have a complete seachange in user perception and adoption of desktop linux?"  Alan, with a wry grin, answered: "Do you want the controversial answer? ... Stop making other distros and only focus on Ubuntu."

Now that comment by Alan caused some people to get their panties in a real twist.  How dare someone that works at Canonical suggest (in a joking manner) that we should only be presenting Ubuntu as an option to users.  Well, I have plenty to say about the people that are upset by what Alan said, but I'm going to table that discussion for just a moment, because first I want to give my thoughts on what Alan said.  

### Too much choice, too much freedom

I one hundred percent agree with what Alan said.  In fact, I have been practicing what Alan said since I switched to Linux on the desktop many years ago.  When people come to Linux, one of the things that scares them the most is choice.  They don't understand it.  They've never been presented the kind of choice, the kind of freedom, that we have in Linux.  That choice--that freedom--can be paralyzing to the new Linux user.  

The new Linux user suddenly have hundreds of choices as far as a distribution to run, hundreds of choices as far as the desktop environment or window manager to use, and so on.  How could anyone that is completely uninformed about Linux, and the software that runs on Linux, make a rational decision on what they should be using?  

The short answer--they can't make that decision.  You, as the person presenting Linux to them, should make that decision for them.  And the distibution that you should give them should be Ubuntu.  It is the distribution that I typically install on any new Linux user's machine.  

### Why Ubuntu?

And the reasons why you give them Ubuntu?  Ubuntu is, by far, the most popular Linux distro for desktop Linux.  Now popularity itself is not necessarily THE reason you want to give users Ubuntu, but some of the side effects of that popularity Ubuntu an attractive choice for newbies.

Due to its popularity, when a new Linux user searches for an answer to a Linux question, 95 times out of 100, the documentation that they find online will be about Ubuntu.  The Ubuntu forums are massive and have millions of posts.  The AskUbuntu stack exchange is also massive.  So many hosting companies offer Ubuntu servers, so much of their documentation is Ubuntu-centric.  And with so many people using Ubuntu, asking a question in a forum or in IRC is likely to get answered more quickly than asking a question about Joe's OS that just launched last week on SourceForge.

Another plus for Ubuntu is that they have an LTS version, which stands for Long Term Support.  And this is the version of Ubuntu that I put on people's machines, and I recommend you do the same.  Ubuntu LTS releases are supported up to ten years!  If I never see that person or their computer again, I know that the OS that I put on that computer will probably outlast the computer.

Also, Ubuntu is a static release model distribution rather than a rolling release distro.  That means that there are a lot less updates, which means that breakages should be much rarer.  And honestly, most computer users never update anyway.  And on Ubuntu, that's fine.  If they go months or even years without updating Ubuntu LTS, their machine will still run just fine.  And if I happen to see this person again and notice that they have a massive update pending, then I might take care of that for them, just in case anything does break.  

By contrast, rolling releases must be updated frequently.  Going weeks or months without updating is very bad because when you finally do update, you will have hundreds or even thousands of packages needing to update.  The potential for breakage is much greater.

So, what I have always done is I keep a USB key with the latest Ubuntu LTS on it.  If someone asks about Linux, I can show them Ubuntu on the live USB.  If they like what they see, I can install it for them.  I never mention other distros, not even the distro that I run--by the way, I don't run Ubuntu.  I never ever show them DistroWatch.  To a new user, DistroWatch shouldn't exist.  Actually for any Linux user, DistroWatch shouldn't exist but that's another story.

The best recipe for success is to present the new user one distro with one desktop and just leave it at that.  Perhaps, they will do some research and find out that their are other distros and other desktop environments.  But let them work that out on their own.  If you present all of that to them on the first day, you will lose them.  They will see Linux as a disorganized cabal and go running back to Windows or Mac.

### Don't push your favorite distro!

And you should never push your own distro onto the new Linux user.  This isn't a team sport.  You don't win anything by converting someone over to your distro.  And I think the people with this "my team versus your team" mindset are the ones that are upset at Alan Pope's comments, and they will likely be upset at my thoughts on the matter.

If you are a Red Hat fanboy, installing Ubuntu on a new user's machine does not reflect, in any way, what you think about Red Hat as a company or Canonical as a company.  If you use Arch, by the way, installing Ubuntu for someone doesn't reflect poorly on you or on Arch.  The distro fanboys do more harm to our community than any other group because they actually do drive people away.  

If you think that the quality of your Linux distro is directly related to how many people use it, then you have some mental issues that you really need to work out.  And I'm being serious here.  If you need others to validate your Linux distro choice, then something is not right with you, and it's probably not tech-related.

So burn those USB keys! Stick them in your laptop bag or put them on your keychain.  And when you come across a potential Linux user, you will be prepared to help them start a successful Linux journey. 
